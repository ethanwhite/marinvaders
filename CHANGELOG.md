# CHANGELOG

## 0.3.1 (20210622)

### Bugfix

- Fixed API calls to WoRMS
- Consistent dtype int for aphiaID and ECO_CODE columns

## 0.3.0 (20210618)


### Additional functionality

- added gisd_scraper for reading and converting data from iucn-gisd

### Breaking changes

- IUCN data (GISD and redlist) becomes optional
- MarineLife.aliens renamed to MarineLife.alien_species (consitent with .all_species)
- MarineLife.affected_by_invasive only available when specifying redlist data
- MarineLife.all_species now returns a dataframe
- MarineLife and Species classes "obis" and "observations" became private ("_obis" and "_observations")
- marinelife.py renamed to main.py

### Intern

- Refactored GISD data reader
- Refactored redlist data reader (was taxonomy reader before)
- Removing lru_cache
- Multiple bug fixes

### Documentation

- Added section on IUCN data
- More details on licenses of the underlying data in the readme
- Documentation now only builds on the master branch or with a manual build
- DataFrame table header reference
- Added binder links to readme and notebook


## 0.2.3


### Docs

- finalized docs 
- added CI for doc generation - available at https://marinvaders.gitlab.io/marinvaders/
- moved tutorial notebook into the doc folder

### Tests

- added coveralls integration
- increased test coverage

## 0.2.2 (20210319)

Minor fixes to get it into conda-forge.

## 0.2.1 (20210319)

Aborted version due to PyPI failed upload

## 0.2.0 (20210319)

### API breaking changes

- hide "reported_as_aliens_and_natives" method
  
### Docs

- added mkdocs explaining the data background
- cleaned readme.md

## 0.1.0 (20210308)

### API breaking changes

- renamed reported_as_aliens_and_natives to reported_as_alien_and_native 
- renamed reported_as_aliens to reported_as_alien 
- renamed obis_elsewhere to all_occurrences 
- Added marinevaders.marinelife imports in top-level init 

### Extended fuctionality

- Species class accepts urn string 

### Development

- added CHANGELOG
- integration test with all eco-regions 
- reformated tests 
- refactored obis_elsewhere to avoid deprecation warning 

### Documentation

- reformated readme 
- refactored tutorial notebook 

## v0.0.12 (20210223)

### Bugfixes

- Typos
- Removing duplicates in in affected by invasive 

### Data

- Renamed natcon to molnar

### Development

- General refactoring

## v0.0.11 (20210211)

### Data

- New taxonomy file
- 
### Development

- Removing obsolete scripts
- 
### Bugfixes

- Typos

## Earlier versions

- see git history
