# Installation

There are several ways to get the MarINvaders toolkit running.

## Running in the cloud through Binder

If you just want to try it, the easiest possibility is to start the tutorial in the [Binder](https://mybinder.org/) cloud:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/marinvaders%2Fmarinvaders/master?filepath=%2Fdocs%2Fmarinvaders.ipynb)


## Local installation

MarINvaders is registered at PyPI and at conda-forge for installation within a conda environment.
To install use

=== "pip"

    ``` bash
    pip install MariINvaders --upgrade
    ``` 

=== "conda"

    ``` bash
    conda update -c conda-forge MarINvaders
    ``` 


We recommend to use [(ana)conda](https://www.anaconda.com/products/individual) [environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) or [Python virtual environments](https://docs.python.org/3/tutorial/venv.html) for that:


=== "Virtual Environment"

    !!! note
        Minimum Python version: 3.7

    In the terminal create a new virtual environment inside a directory you want to use the toolkit:

    
    ``` bash
    python3 -m venv env
    ``` 

    This will create new directory **venv**

    Activate the virtual environment:

    ``` bash
    source env/bin/activate
    ``` 
    and install the toolkit:

    ``` bash
    pip install MarINvaders --upgrade
    ``` 


=== "(Ana)conda Environment"

    !!! note
        Minimum conda version: 4.6

    In the terminal create a new conda environment:

    ``` bash
    conda create env --name marinvaders -c conda-forge marinvaders
    ```

    This will create new environment marinvaders with marinvaders already installed.

    To use the environment with

    ``` bash
    conda activate marinvaders
    ```




## Development installation

For development, clone the [repository](https://gitlab.com/marinvaders/marinvaders).
In the repository you will find a file * environment_dev.yml * which contains the specs for the development environment.
You can set this up locally by

    `conda create env -f environment_dev.yml`

The name of the resulting environment is * marinvaders_dev *, activate it with

    `conda activate marinvaders_dev`

Please also check for [Contribution
documentation](https://marinvaders.gitlab.io/marinvaders/contributing/) for
further development guidelines.
