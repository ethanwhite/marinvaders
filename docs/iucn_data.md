# Optional IUCN data

Data provided by the [IUCN (International Union for Conservation of Nature)](https://www.iucn.org/) **is not allowed to be redistributed** . There are, therefore, some manual steps needed to include this data in the analysis.

!!! note
    Although this data is not essential for using MarINvaders we recommend to add it as it provides additional data on alien ranges (GISD) and allows to assess which species are affected by aliens (IUCN Red list) - see also the [Data Background Section](data_background.md).

## GISD data

MarINvaders includes a scraper and parser for the [GISD](https://www.iucngisd.org/gisd/) data.
This can be found in the util directory in the source code and is available upon installation through

```Python
import marinvaders.util
marinvaders.util.get_gisd('/some/dir/gisd_data.json')
```

Due to repeated calls to the GISD webpage getting the whole data takes about 30-60min.
However, this only needs to be done once and the downloaded data can be reused.

To use the GISD data in subsequent MarINvader calls, pass the file location to the initialization of the analysis classes.

For example, to analyse the distributions of the [Red Lionfish, *Pterois volitans*](https://en.wikipedia.org/wiki/Red_lionfish) - [Aphia ID = 159559](http://www.marinespecies.org/aphia.php?p=taxdetails&id=159559).

```Python
import marinvaders
lionfish = marinvaders.Species(aphia_id=159559, gisd_file='/some/dir/gisd_data.json')
lionfish.reported_as_alien
```

This reports over 60 region where this species is reported as alien. 
We can also see which datasets report the species in a certain region (column 
*dataset*).

We can then compare the alien ranges found in by using GISD to the ones based solely on WoRMS and NatCon with


```Python
lionfish_wogisd = marinvaders.Species(aphia_id=159559)
additional_ecoregions = set(lionfish.reported_as_alien.ECOREGION
                            ).difference(
                            set(lionfish_wogisd.reported_as_alien.ECOREGION))
```

which results in 22 additional regions with reports of alien Red Lionfish.


Similarly, we can pass the GISD data to the regional analyse class:

```Python
import marinvaders
cocos = marinvaders.MarineLife(eco_code=20169, gisd_file='/some/dir/gisd_data.json')
cocos.alien_species
```

## IUCN Red List data

Data from the [IUCN Red list](https://www.iucnredlist.org/) can be searched and downloaded [following this link](https://www.iucnredlist.org/search).
Note that a log-in is required (registration is free of charge).

The search parameters to get all species affected by marine invaders are:


| Setting                            |                                            Value |
| ---------------------------------- | -----------------------------------------------: |
| Type                               |                                          Species |
| Search Filters - Red List Category |                                               CR |
| "                                  |                                               DD |
| "                                  |                                               EN |
| "                                  |                                      LC or LR/lc |
| "                                  |                                            LR/cd |
| "                                  |                                      NT or LR/nt |
| "                                  |                                               VU |
| Search Filter - Threats            | 8.1 – Invasive non-native/alien species/diseases |
| Search Filter - Habitats           |                               9 – Marine neritic |
| "                                  |                              10 - Marine oceanic |
| "                                  |                         11 - Marine Deep Benthic |
| "                                  |                           12 - Marine Intertidal |
| "                                  |                    13 - Marine Costal/Supratidal |
| Search Filter - Include            |                                          Species |
| Geographical Scope                 |                                           Global |

We can then download the information available (drop down menu “Download” - “Search results”) for all marine species threatened by invasive species.
After issuing a search and agreeing on the terms-of-service, the results can be downloaded from the account page after a couple of minutes.

The download is provided in zip format, after extracting there are some licence and readme files as well as one *assessment.csv*.
This file contains the scientific names of species threatened by invasives and can be processed as is by the MarINvaders package.

```Python
import marinvaders
ne_sulawesi = marinvaders.MarineLife(eco_code=20133, 
                                     redlist_file='/some/dir/redlist/assessments.csv')
ne_sulawesi.affected_by_invasive
```

Alternatively, it is also possible to specify the red-list data-file at a later stage (NB: the attribute 'affected_by_invasive' only becomes available after specifying some red-list data):


```Python
redlist_file='/some/dir/redlist/assessments.csv'
ne_sulawesi = marinvaders.MarineLife(eco_code=20133)
ne_sulawesi.specify_redlist_data(redlist_file)
```

Is it also possible to specify alternative redlist data (e.g. after modifying the search parameters above) and set the result attribute names accordingly:

```Python
ne_sulawesi = marinvaders.MarineLife(eco_code=20133)
ne_sulawesi.specify_redlist_data(redlist_file1, result_attribute_name='affected1')
ne_sulawesi.specify_redlist_data(redlist_file2, result_attribute_name='affected2')
```

This then makes the cross-reference of the redlist_file1 data with the species found in the eco-region available at
```Python
ne_sulawesi.affected1
```

and the ones from redlist_file2 in

```Python
ne_sulawesi.affected2
```
