## Contributing to marinvaders

Thank you for taking time to contribute!

## Suggesting a feature

If you've got a good idea for a feature please let us know!

When suggesting a feature:

* Check the code on GitLab to make sure it's not already hiding in an unreleased version.
* Considered if it's necessary in the library, or is an advanced technique that could be separately explained in an example.
* Check [existing issues, open and closed,](https://gitlab.com/marinvaders/marinvaders/-/issues?scope=all&state=all) to see if it has been discussed before. Feel free to reopen issues at any time.

## Filing a bug report

If you're having trouble with our code then please [raise an issue to let us know](https://gitlab.com/marinvaders/marinvaders/-/issues). Be as detailed as possible and be ready to answer questions when we get back to you.

## Submitting a pull request

If you've decided to fix a bug - even something as small as a single-letter typo - great! Anything that improves the code/documentation for future users is highly appreciated.

If you decide to work on a  requested feature it's best to let us (and everyone else) know what you're working on to avoid any duplication of effort. You can do this by replying to the original Issue for the request.

When contributing a new example or making a change to a library please keep your code style consistent with ours. We follow the [pep8 guidelines for Python](https://www.python.org/dev/peps/pep-0008/). See the *.gitlab_ci.yml* for the tests on code quality and style which need to pass for a merge. 

Ideally, your contribution will increase the test coverage. The existing tests 
cover most of the code for data which we can automatically process. 
MarINvaders, however, can also process data which [requires manual download and 
can not be included](https://marinvaders.gitlab.io/marinvaders/iucn_data/) in the open source repository. To test for 
these cases and all available ecoregions run the *tests/integration_evaluation.py* script. Be prepared that this can run for several days.
 
The repository includes a [conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) for development. See the [Installation documentation](https://marinvaders.gitlab.io/marinvaders/install/#development-installation) for further information. 

#### Do

* Do use pep8 style guidelines
* Do comment your code where necessary
* Do submit only a single example/feature per pull-request
* Do include a description of what your example is expected to do
* Do include a test case for the new functionality
* Do add details of your example to examples/README.md if it exists

#### Don't

* Don't include any license information in your examples or contributions - MarINvasders code is GNU GPLv3 licensed and the data keep is original license assigned by the data provider
* Don't try to do too much at once - submit one examples/bug-fix at a time, and be receptive to feedback
* Don't submit multiple variations of the same example, demonstrate one thing concisely

### If you're submitting an example

Try to do one thing and do it concisely. Keep it simple. Don't mix too many ideas.

The ideal example should:

* demonstrate one idea, technique or API as concisely as possible in a single Python script
* work out of the box if possible - although sometimes configuration is necessary
* be well commented and attempt to teach the user how and why it works
* document any required configuration, and how to install dependencies, etc


### Licensing

When you submit code to our libraries you implicitly and irrevocably agree to adopt the associated licenses. You find this in the file named LICENSE.


### Submitting your code

Once you're ready to share your contribution with us you should submit it as a Pull Request.

* Be ready to receive and embrace constructive feedback.
* Be prepared for rejection; we can't always accept contributions. If you're unsure ask first through the [Issues board](https://gitlab.com/marinvaders/marinvaders/-/issues)!

## Thank you!

If you have any questions, concerns or comments about these guidelines, please get in touch. 
You can do this by [raising an issue](https://gitlab.com/marinvaders/marinvaders/-/issues).

Above all else, we hope you enjoy yourself, learn things and make and share great contributions.

Happy hacking!

-- The [IEDL](https://iedl.no) Crew
